package ru.siniy_kit.forvadim;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;

import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.io.IOException;

import static android.R.attr.bitmap;

public class MainActivity extends Activity {

    final int CAMERA_CAPTURE = 1;
    final int GALLERY_REQUEST = 2;
    private Uri picUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
    }

    public void makePhoto(View v) {
        try {
            // Намерение для запуска камеры
            Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(captureIntent, CAMERA_CAPTURE);
        } catch (ActivityNotFoundException e) {
            // Выводим сообщение об ошибке

            Toast.makeText(this, getString(R.string.cant_make_photo), Toast.LENGTH_SHORT).show();
        }
    }

    public void fromGallery(View v) {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, GALLERY_REQUEST);
    }

    public void sendFile(View v){
//        new FilesUploadingTask(getPath()).execute();
    }

    public String getPath(Uri uri) {
        String[] projection = { MediaStore.Images.Media.DATA };
        @SuppressWarnings("deprecation")
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {

            switch (requestCode) {
                // Вернулись от приложения Камера
                case CAMERA_CAPTURE:
                    // Получим Uri снимка
                    picUri = data.getData();

                    Bundle extras = data.getExtras();
                    // Получим изображение
                    Bitmap thePic = extras.getParcelable("data");
                    // передаём его в ImageView
                    ImageView picView = (ImageView) findViewById(R.id.picture);

                    picView.setImageBitmap(thePic);
                    break;
                // Получаем из галереи
                case GALLERY_REQUEST:
                    Bitmap bitmap = null;
                    ImageView imageView = (ImageView) findViewById(R.id.picture);
                    Uri selectedImage = data.getData();
                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), selectedImage);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    imageView.setImageBitmap(bitmap);
                    Toast.makeText(getApplicationContext(),getPath(selectedImage),Toast.LENGTH_LONG).show();
                    //new FilesUploadingTask(getPath(selectedImage)).execute();
                    break;
                default:
                    break;
            }

        }
    }

}